

const Task = require("../models/Task");

// Create a task without duplicate names
module.exports.createTask = (req, res) => {
	console.log(req.body)

	Task.findOne({name: req.body.name}).then(result => {
		console.log(result);

		if(result !=null && result.name == req.body.name){
			return res.send("Duplicate task found!")
		}
		else {
			let newTask = new Task({
				name: req.body.name
			})
			newTask.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));
		}
	})
	.catch(error => res.send(error))
};

// Retrieve ALL Tasks
module.exports.getAllTasksController = (req, res) => {
	Task.find({})
	.then(tasks => res.send(tasks))
	.catch(error => res.send(error));
}

// Retrieve Single Task
module.exports.getSingleTaskController = (req, res) => {
	console.log(req.params);

	Task.findById(req.params.taskId)
	.then(result => res.send(result))
	.catch(error => res.send(error));
}

// Updating task status
module.exports.updateTaskStatusController = (req, res) => {
	console.log(req.params.taskId);
	console.log(req.body);

	let updates ={
		status: req.body.status
	};
	Task.findByIdAndUpdate(req.params.taskId, updates, {new: true})
	.then(updatedTask => res.send(updatedTask))
	.catch(error => res.send(error));
}

module.exports.deleteTaskController = (req, res) => {
	console.log(req.params.taskId);

	Task.findByIdAndRemove(req.params.taskId)
	.then(removedTask => res.send(removedTask))
	.catch(error => res.send(error));
}