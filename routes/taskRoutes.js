const express = require("express");
const router = express.Router();

const taskControllers = require("../controllers/taskControllers");

console.log(taskControllers);

// Create task routes
router.post("/", taskControllers.createTask);

// View ALL TASKS route
router.get("/allTasks",taskControllers.getAllTasksController);

// Get a single task
router.get("/getSingleTask/:taskId", taskControllers.getSingleTaskController);

// Update a task status
router.patch("/updateTask/:taskId", taskControllers.updateTaskStatusController);

// Delete a task
router.delete("/deleteTask/:taskId", taskControllers.deleteTaskController);

// Used in the server
module.exports = router;